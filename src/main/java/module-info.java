module com.taskapp.taskapp {
    requires javafx.controls;
    requires javafx.fxml;

    requires org.controlsfx.controls;
    requires com.dlsc.formsfx;
    requires org.kordamp.ikonli.javafx;

    opens com.taskapp.taskapp to javafx.fxml;
    exports com.taskapp.taskapp;
}